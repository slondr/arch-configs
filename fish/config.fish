if status --is-login
  if test -z "$DISPLAY" -a $XDG_VTNR = 1
    exec startx -- -keeptty
  end
end

export EDITOR="emacsclient -c -F \"'(fullscreen . fullboth)\" -e '(switch-to-buffer \"*dashboard*\")'"
