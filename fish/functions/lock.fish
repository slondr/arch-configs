function lock
    if ! type -q i3
	# TODO: Popup message saying i3 is not installed.
    end
	
    if type -q i3lock
	i3lock -i (random_file $HOME/.config/lock-images)
    end

    # TODO: Popup message saying i3lock is not installed.
end
