#!/usr/bin/env bash

update_system () {
    sudo pacman -Syu
}

install_pacaur () {
    # Install Pacaur Deps
    sudo pacman -S binutils expac fakeroot gcc git make openssh yajl --noconfirm

    # Make Temp Dir
    mkdir -p /tmp/pacaur_install
    cd /tmp/pacaur_install

    # Install Cower Dep
    if [ ! -n "$(pacman -Qs cower)" ]; then
	curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=cower
	makepkg PKGBUILD --skippgpcheck --install --needed
    fi

    # Install "pacaur" from AUR
    if [ ! -n "$(pacman -Qs pacaur)" ]; then
	curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=pacaur
	makepkg PKGBUILD --skippgpcheck --install --needed
    fi

    cd $HOME
    rm -rf /tmp/pacaur_install
}

install_editor () {
    sudo pacman -S emacs
    export EDITOR="emacs"

    cd $HOME
    rm -rf $HOME/.emacs.d
    git clone https://gitlab.com/gluaxspeed/emacs-conf.git .emacs.d
}

install_rust_sys_tools () {
    pacaur -S bat bottom dust exa fd procs ripgrep rm-improved tealdeer tokei
}

install_sys_tools () {
    pacaur -S flameshot hunspell hunspell-en_US hyperfine imagemagick jq texlive-most man-db man-pages
}

install_shell () {
    pacaur -S fish
    source $HOME/.config/fish/config.fish
}

install_desktop_env () {
    pacaur -s dunst i3lock i3-gaps i3status-rust rofi xorg-server xorg-xinit xrandr
}

install_audio () {
    pacaur -S pulseauido pulseaudio-alsa alsa-plugins alsa-utils
}

install_fonts () {
    pacaur -S xorg-font-util xorg-mkfontscale ttf-opendyslexic powerline-common powerline-fonts
}

install_configs () {
    cd $HOME
    rm -rf $HOME/.config
    git clone https://gitlab.com/gluaxspeed/arch-configs.git .config

    systemctl enable --user emacs
    systemctl start --user emacs
}

install_programming_languages () {
    # deno, go, nodejs, cl
    pacaur -S deno go nodejs sbcl yarn

    # quicklisp
    curl -O https://beta.quicklisp.org/quicklisp.lisp
    
    sbcl --load quicklisp.lisp \
       --eval '(quicklisp-quickstart:install)'       \
       --eval '(ql:add-to-init-file)'                \
       --eval '(quit)'

    rm quicklisp.lisp

    # rustup
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
    source $HOME/.cargo/env

    # rust-analyzer
    pacaur -S rust-analyzer
}

configure_system () {
    install_system
    install_pacaur
    install_editor
    install_shell
    install_rust_sys_tools
    install_sys_tools
    install_fonts
    install_audio
    install_desktop_env
    install_programming_languages
    install_configs
}

configure_system
